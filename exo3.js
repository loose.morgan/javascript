
let read = require("readline-sync");
let prixVente = read.questionFloat("Quel est le prix de vente du produit ?");
let coutFabrication = read.questionFloat("Quel est son coût de fabrication");
let ecart = prixVente - coutFabrication

if (prixVente >= coutFabrication) 
{

    console.log("Profit de : " + ecart +" €");
}else
{
    console.log("Perte de : " + ecart + " €");
}