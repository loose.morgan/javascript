// Vous disposez d'une liste de nombre.
// Triez la liste du plus petit au plus grand
// Résultat attendu
// [12, 25, 5, 7, 6, -5]
// -->[-5, 5, 6, 7, 12, 25]

let liste = [12, 25, 5, 7, 6, -5];

let tab = [];

for (let i = 0; i < liste.length; i++) {}
console.log("Tableau avant : ");
console.log(liste);

function tri(liste) {
  for (var i = 0; i < liste.length; i++) {
    var min = i;
    for (var j = i + 1; j < liste.length; j++) {
      if (liste[j] < liste[min]) {
        min = j;
      }
    }
    let temp = liste[i];
    liste[i] = liste[min];
    liste[min] = temp;
  }
  return liste;
}
tri(liste);
console.log("Tableau après : ");
console.log(liste);
