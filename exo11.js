// Écrivez un programme qui demande à l'utilisateur de saisir un nombre décimal.
// Le programme doit renvoyer exactement le nombre de billets et de pièces qu'il faut pour obtenir ce nombre
// let read = require("readline-sync");

// let billetCinqCent = 0;
// let billetDeuxCent = 0;
// let billetVingt = 0;
// let billetDix = 0;
// let billetCinq = 0;
// let pieceDeux = 0;
// let pieceCinquanteCent = 0.00;
// let pieceDeuxCent = 0.00;

// let renduArgent = read.questionFloat("Saisissez un nombre décimal : ");

// while (renduArgent > 0) {
//   if (renduArgent >= 500) {
//     billetCinqCent = billetCinqCent + 1;
//     renduArgent = renduArgent - 500;
//   } else if (renduArgent >= 200) {
//     billetDeuxCent = billetDeuxCent + 1;
//     renduArgent = renduArgent - 200;
//   } else if (renduArgent >= 20) {
//     billetVingt = billetVingt + 1;
//     renduArgent = renduArgent - 20;
//   } else if (renduArgent >= 10) {
//     billetDix = billetDix + 1;
//     renduArgent = renduArgent - 10;
//   } else if (renduArgent >= 5) {
//     billetCinq = billetCinq + 1;
//     renduArgent = renduArgent - 5;
//   } else if (renduArgent >= 2) {
//     pieceDeux = pieceDeux + 1;
//     renduArgent = renduArgent - 2;
//   } else if (renduArgent >= 0.5) {
//     pieceCinquanteCent = pieceCinquanteCent + 1;
//     renduArgent = renduArgent -0.5;
//   } else if (renduArgent >= 0.02) {
//     pieceDeuxCent = pieceDeuxCent + 1;
//     renduArgent = renduArgent -0.02;
//   }
// }
// console.log("nb de billet de 500€ rendu(s) : " + billetCinqCent);
// console.log("nb de billet de 200€ rendu(s) : " + billetDeuxCent);
// console.log("nb de billet de 20€ rendu(s) : " + billetVingt);
// console.log("nb de billet de 10€ rendu(s) : " + billetDix);
// console.log("nb de billet de 5€ rendu(s) : " + billetCinq);
// console.log("nb de pièce de 2€ rendu(s) : " + pieceDeux);
// console.log("nb de pièce de 0.5€ rendu(s) : " + pieceCinquanteCent);
// console.log("nb de pièce de 0.02€ rendu(s) : " + pieceDeuxCent);
let read = require("readline-sync");

let num = read.questionFloat("Quelle somme souhaitez-vous retirer ? ");

let monnaie = [
  500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01,
];

function distributeur(num) {
  let reste = num;

  for (i = 0; i < monnaie.length; i++) {
    reste = (num % monnaie[i]).toFixed(2);

    let sommeADiviser = (num - reste).toFixed(2);

    let nombreMonnaie = parseInt(sommeADiviser / monnaie[i]);

    if ((monnaie[i] > 2) & (nombreMonnaie !== 0)) {
      console.log(`Billet  ${monnaie[i]} € : ${nombreMonnaie}`);
    } else if ((monnaie[i] <= 2) & (nombreMonnaie !== 0)) {
      console.log(`Pièce  ${monnaie[i]} € : ${nombreMonnaie}`);
    }

    num = reste;
  }
}

distributeur(num);
