// Vous connaissez certainement le jeu télévisé de Vincent Lagaff nommé "Le Juste Prix" ?
// Une cagnotte remplie de cadeaux est proposée à un candidat qui arrive en finale.
// Tous ces cadeaux ont une valeur qui est cachée au candidat.
// Le candidat a 30 secondes pour deviner le prix exact du lot avec pour seule informations :
// C'est plus
// C'est moins
let read = require("readline-sync");

let nbADeviner = Math.floor(Math.random() * 1000) + 1;

let cpt;

for (cpt = 1; cpt <= 30; cpt += 1) {
  let nombre = read.questionInt("Saisissez un nombre : ");
  if (nombre === nbADeviner) {
    console.log("Félicitations");
    break;
  }
  if (nombre > nbADeviner) {
    console.log("C'est moins !");
  }
  if (nombre < nbADeviner) {
    console.log("C'est plus !");
  } 
 
}
console.log("c'est fini. le chiffre cherche est : " + nbADeviner);