// Vous disposez d'une liste de températures (°C).
// Parcourez cette liste et affichez la température la plus proche de 0°C.
// Si une température négative est aussi proche de 0 qu'une température positive, la valeur négative prend le dessus.
// [12, 25, 5, 7, 6, -5]
// La température la plus proche de 0°C est -5°C.

let temperatures = [12, 25, 5, 7, 6, -5];
let tempMin = temperatures[0];

function compare(temperatures) {

  for (i = 0; i < temperatures.length; i++) {
    if (temperatures[i] < tempMin) {
      tempMin = temperatures[i];
    }
  }
  return tempMin;
}
compare(temperatures);
console.log(tempMin);
