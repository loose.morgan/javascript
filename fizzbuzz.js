// Remplacez tous les multiples de 3 par "FIZZ"
// Remplacez tous les multiples de 5 par "BUZZ"
// Remplacez tous les multiples de 3 ET 5 par "FIZZBUZZ"

for (let i = 1; i <= 20; i++) {
  if (i % 15 == 0) console.log("FizzBuzz");
  else if (i % 3 == 0) console.log("Fizz");
  else if (i % 5 == 0) console.log("Buzz");
  else console.log(i);
}
