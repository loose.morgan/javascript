// Adaptez l'exercice 8 pour faire descendre l'escalier dans l'autre sens afin de former une pyramide

let read = require("readline-sync");

let nb = read.questionInt("Saisissez un nombre : ");

let etoile = "*";

function escalier(nb) {
  for (i = 1; i <= nb; i++) {
    console.log(etoile.repeat(i));
  }

  for (i = nb; i >= 0; i--) {
    console.log(etoile.repeat(i));
  }
}

escalier(nb);
