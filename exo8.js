//Écrivez un programme qui demande à un utilisateur un nombre puis qui génère un escalier d'étoiles.

let read = require("readline-sync");

let nb = read.questionInt("Saisissez un nombre : ");

let etoile = "*";

function escalier(nb) {
  for (i = 1; i <= nb; i++) {
    console.log(etoile.repeat(i));
  }

  console.log("\n");
}

escalier(nb);
