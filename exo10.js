// Adaptez l'exercice 9 pour que la pyramide soit dans le bon sens (^)
let read = require("readline-sync");

let nb = read.questionInt("Saisissez un nombre : ");
let etoile = "";

function pyramid(nb) {
  for (let i = 0; i < nb; i++) {
    let espace = "";
    for (let j = 0; j < nb - i - 1; j++) {
      espace += " ";
    }
    for (let k = 1; k <= 2 * i + 1; k++) {
      etoile += "*";
    }
    console.log(espace + etoile);
    etoile = "";
  }
}
pyramid(nb);
