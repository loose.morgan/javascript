//Écrivez un programme qui demande à l'utilisateur de saisir 3 nombres puis trouver le plus grand.
let read = require("readline-sync");
let cpt=0;
let tableau = [];
let valeurMax;
while (cpt!==3) {
    let nb = read.questionInt("Saisissez un nombre :");
    tableau.push(nb);
    cpt++;
}
console.log(tableau);
valeurMax = compare(tableau);
console.log(valeurMax);
function compare(tableau)
{
  var valeurMax = tableau[0];
  for (i=0; i<tableau.length; i++)
  {
    if (tableau[i] > valeurMax)
      {
        valeurMax = tableau[i];
      }
  }
  return valeurMax;
}