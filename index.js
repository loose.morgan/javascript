console.log ("Hello World !");
console.log (`Mon prénom est : ${prenom}`);
//les différents types en JS
//String
var prenom = "Morgan";
var texte = "voici \"du\ texte";
//Integer
var age = 29;
//Boolean
var isOk = true;
var isNotOk = false;
//Any
var a;

// les types d'opérateurs
// arithmétiques : 
var a = 12;
var b = 4; 
 console.log(a + b);
 console.log(a - b);
 console.log(a / b);
 console.log(a * b);
 console.log(a % b);
 console.log(++a);
 console.log(a++);
 console.log(--a);
 console.log(a--);
 
//opérateurs de comparaison - renvoie soit vrai soit faux

 console.log (a < b);
 console.log (a > b);
 console.log (a >= b);
 console.log (a <= b);

//comparaison valeur 
 console.log (a == b);
//comparaison valeur + type 
 console.log (a === b);
 console.log (a !== b);
